<?php

/**
 * Class DbService
 *
 * @author Andriy Lekh <lehandriy@gmail.com>
 */
class DbService
{
    /**
     * @var array
     */
    private $configs = [];

    /**
     * DbService constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $configs = \json_decode(\file_get_contents('config.json'), true);
        if (!isset($configs['db'])) {
            throw new \Exception('Please check config.json');
        }

        $this->configs = $configs['db'];
    }

    /**
     * Get PDO instance
     *
     * @param bool $createDatabase
     *
     * @return PDO
     */
    public function getPdo($createDatabase = false): \PDO
    {
        try {
            $pdo = new \PDO(
                \sprintf("mysql:host=%s", $this->configs['server']),
                $this->configs['username'],
                $this->configs['password']
            );

            if (!$createDatabase) {
                $pdo->query(\sprintf('use `%s`', $this->configs['db_name']));
            }

            return $pdo;
        } catch (\Exception $e) {
            die("Check your database connection.\r\n");
        }
    }

    /**
     * Get PDO for create database
     */
    public function prepareDb()
    {
        $pdo = $this->getPdo(true);

        // Create database
        $pdo->query(
            \sprintf(
                "CREATE DATABASE IF NOT EXISTS `%s` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;",
                $this->configs['db_name']
            )
        );
        $pdo->query(\sprintf('use `%s`', $this->configs['db_name']));

        // Create tables
        $pdo->query("
            CREATE TABLE `dictionary` (
                `word` VARCHAR(255) NOT NULL COMMENT '',
                PRIMARY KEY (`word`)  COMMENT ''
            );
          
            CREATE TABLE IF NOT EXISTS `watchlist` (
                `word` VARCHAR(255) NOT NULL COMMENT '',
                PRIMARY KEY (`word`)  COMMENT ''
            );
        ");

        echo("Your database successfully prepared.\r\n");
    }
}
