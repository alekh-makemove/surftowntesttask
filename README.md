# Surftown test task

This project provides the way to count unique words in file, and save result into database.

## Usage
First of all you need to edit config file(`config.json`) with your database config.

Then you are able to use console.
For using counter you need to prepare new database and tables. For preparing them you can use this command:
```
$ php console prepare-db
```

#### Counter

For run counter for some file you should run next command:
```
$ php console run-counter <filepath>
```
Example files with text are in `files` folder:
> files/100000.txt - file with 100000 unique words  
> files/500000.txt - file with 500000 unique words  
> files/lorem-ipsum.txt - file with "Lorem Ipsum" text with 4500 words

#### Whatchlist
You are able to add some specific words into watchlist.  
For adding new words you should create file with words for watchlist and run next command:
```
$ php console update-watchlist <filepath>
```
Example watchlist file is in `files/watchlist.txt`

#### List of commands:
For check list of commands just run:
```
$ php console
``` 

#### Performance
> After executing any command you will see time report in output.
```
Total process time(seconds): 7
```
For me processing half a million unique words(`files/5000000.txt`) takes around 6-8 seconds.
