<?php

require_once 'DbService.php';

/**
 * Class WatchlistService
 *
 * @author Andriy Lekh <lehandriy@gmail.com>
 */
class WatchlistService
{
    /**
     * @var integer
     */
    const ARRAY_CHUNK_SIZE = 100000;

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * WatchlistService constructor.
     */
    public function __construct()
    {
        $dbService = new DbService();
        $this->pdo = $dbService->getPdo();
    }

    /**
     * Get matched words with output in console
     *
     * @param $wordsFromFile
     */
    public function getMatchedWordsOutput($wordsFromFile)
    {
        $watchlistWords = $this->getMatchedWords($wordsFromFile);

        $output = "Watchlist words: (" . \count($watchlistWords) . ")\r\n";
        foreach ($watchlistWords as $word) {
            $output .= $word . "\r\n";
        }
        $output .= "\r\n";

        echo($output);
    }

    /**
     * Update watchlist table with new words
     *
     * @param $wordsFromFile
     */
    public function updateWatchlist($wordsFromFile)
    {
        $this->pdo->query("TRUNCATE `watchlist`");

        $arrayChunks = \array_chunk($wordsFromFile, self::ARRAY_CHUNK_SIZE);
        foreach ($arrayChunks as $wordsFromFile) {
            $insertParams = \str_repeat('(?), ', \count($wordsFromFile) - 1) . '(?)';
            $sql = "INSERT IGNORE INTO `watchlist` (word) VALUES $insertParams";
            $query = $this->pdo->prepare($sql);
            $query->execute($wordsFromFile);
        }
    }

    /**
     * Get matched words
     *
     * @param $wordsFromFile
     *
     * @return array
     */
    public function getMatchedWords($wordsFromFile): array
    {
        $words = [];
        $arrayChunks = \array_chunk($wordsFromFile, self::ARRAY_CHUNK_SIZE);
        foreach ($arrayChunks as $wordsFromFile) {
            $inParams = \str_repeat('?, ', \count($wordsFromFile) - 1) . '?';
            $sql = "SELECT w.word FROM watchlist w WHERE w.word IN ($inParams)";
            $query = $this->pdo->prepare($sql);
            $query->execute($wordsFromFile);

            $words = \array_merge($query->fetchAll(\PDO::FETCH_COLUMN, 0), $words);
        }

        return $words;
    }
}
