<?php

/**
 * Class WordService
 *
 * @author Andriy Lekh <lehandriy@gmail.com>
 */
class WordService
{
    /**
     * Get unique words from file
     *
     * @param $filePath
     *
     * @return array
     * @throws Exception
     */
    public function getUniqueWords($filePath): array
    {
        if (!\file_exists($filePath)) {
            throw new \Exception('File not found');
        }

        $fileText = \file_get_contents($filePath);
        $text = $this->prepareText($fileText);

        return $this->filterWords($text);
    }

    /**
     * Prepare text for filtering
     *
     * @param $text
     *
     * @return string
     */
    public function prepareText($text): string
    {
        $text = \strtolower(\trim($text));

        // Remove all special characters
        $text = \preg_replace('/[^a-z0-9 \-(\r\n?|\n|\s\s+)]/', '', $text);

        // Remove line breaks
        $text = \preg_replace('/\r\n?|\n/', ' ', $text);

        // Remove multiple spaces
        $text = \preg_replace('/\s\s+/', ' ', $text);

        // Remove dashes
        $text = \preg_replace('/[-]+ /', '', $text);

        return $text;
    }

    /**
     * Filter text.
     *
     * @param $text
     *
     * @return array
     */
    public function filterWords($text): array
    {
        $words = \explode(' ', $text);

        $preparedWords = \array_filter($words, function ($word) {
            return $word != '';
        });

        $uniqueWords = \array_unique($preparedWords);

        return \array_values($uniqueWords);
    }
}
