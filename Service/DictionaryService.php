<?php

require_once 'DbService.php';

/**
 * Class DictionaryService
 *
 * @author Andriy Lekh <lehandriy@gmail.com>
 */
class DictionaryService
{
    /**
     * @var integer
     */
    const ARRAY_CHUNK_SIZE = 100000;

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * DictionaryService constructor.
     */
    public function __construct()
    {
        $dbService = new DbService();
        $this->pdo = $dbService->getPdo();
    }

    /**
     * Run counter process
     *
     * @param $wordsFromFile
     */
    public function runCounter($wordsFromFile) {
        $newWordsCount = \count($wordsFromFile) - $this->countExistingWords($wordsFromFile);
        echo(\sprintf("%s new words were added. \r\n\r\n", $newWordsCount));

        $this->insertNewWords($wordsFromFile);
    }

    /**
     * Count exists words based on words from file
     *
     * @param $wordsFromFile
     *
     * @return int
     */
    protected function countExistingWords($wordsFromFile): int
    {
        $count = 0;
        $arrayChunks = \array_chunk($wordsFromFile, self::ARRAY_CHUNK_SIZE);
        foreach ($arrayChunks as $wordsFromFile) {
            $inParams = \str_repeat('?, ', \count($wordsFromFile) - 1) . '?';
            $sql = "SELECT * FROM `dictionary` d WHERE d.word IN ($inParams)";
            $query = $this->pdo->prepare($sql);
            $query->execute($wordsFromFile);

            $count += $query->rowCount();
        }

        return $count;
    }

    /**
     * Insert new words in dictionary
     *
     * @param $wordsFromFile
     */
    protected function insertNewWords($wordsFromFile)
    {
        $arrayChunks = \array_chunk($wordsFromFile, self::ARRAY_CHUNK_SIZE);
        foreach ($arrayChunks as $wordsFromFile) {
            $insertParams = \str_repeat('(?), ', \count($wordsFromFile) - 1) . '(?)';
            $sql = "INSERT IGNORE INTO `dictionary` (word) VALUES $insertParams";
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute($wordsFromFile);
        }
    }
}
